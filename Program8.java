//WAP to find the uncommon elements between two arrays.

import java.util.*;
class Demo{

	public static void main(String[] args){

		Scanner sc=new Scanner (System.in);

		System.out.println("Enter the size of First Array:");
		int size1=sc.nextInt();

		int arr1[]=new int[size1];
		System.out.println("Enter the Elements of first Array:");
		
		for(int i=0; i<arr1.length; i++){

			arr1[i]=sc.nextInt();
		}
		
		System.out.println("Enter the size of second Array:");
		int size2=sc.nextInt();

		int arr2[]=new int[size2];
		System.out.println("Enter the Elements of second Array:");
		
		for(int i=0; i<arr2.length; i++){

			arr2[i]=sc.nextInt();
		}

		int flag=0;
		System.out.println("Uncommon elements between arrays :");
		
		for(int i=0; i<arr1.length; i++){

			for(int j=0; j<arr2.length; j++){
		
				if(arr1[i]==arr2[i])
					flag=1;
			}
			if(flag==0)
				System.out.println(arr1[i]);
			flag=0;
		}

		for(int i=0; i<arr1.length; i++){

			for(int j=0; j<arr2.length; j++){

				if(arr2[i]==arr1[i])
					flag=1;
			}
			if(flag==0)
				System.out.println(arr2[i]);
			flag=0;
		}
	}
}
