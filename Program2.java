//WAP to find the number of even and odd integer in a given array of integers
//input: 1  2  5  4  6  7  8
//output: 
//		Number of Even Elements:4
//		Number of Odd Elements: 3

import java.util.*;
class Demo{

	public static void main(String[] args){

		Scanner sc= new Scanner(System.in);
		System.out.println("Enter the size of Array:");
		int size=sc.nextInt();

		int arr[]=new int[size];
		System.out.println("Enter the Elements of Array:");
		for(int i=0; i<arr.length; i++){

			arr[i]=sc.nextInt();
		}
		int count=0,count2=0;
		for(int i=0; i<arr.length; i++){

			if(arr[i]%2==0)
				count++;
			else
				count2++;
		}
		System.out.println("Number of Even Elements:"+ count);
		System.out.println("Number of Odd Elements:"+ count2);
	}
}
