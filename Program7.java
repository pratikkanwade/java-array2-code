//WAP to find the common elements between two arrays.
//input: Enter first array: 1  2  3  5
//		Enter Second array: 2  1  9  8 
//output: commom elements: 1  2

import java.util.*;
class Demo{

	public static void main(String[] args){

		Scanner sc= new Scanner(System.in);
		System.out.println("Enter the size of first array:");
		int size1=sc.nextInt();

		int arr1[]=new int[size1];
		System.out.println("Enter the elements of First Array:");

		for(int i=0; i<arr1.length; i++){

			arr1[i]=sc.nextInt();
		}
		System.out.println("Enter the Size of second Array:");
		int size2 =sc.nextInt();

		int arr2[]=new int[size2];
		System.out.println("Enter the Elements of Second Array: ");
		for(int i=0; i<arr2.length; i++){

			arr2[i]=sc.nextInt();
		}

		System.out.println("Common Elements : ");
		for(int i=0; i<arr1.length; i++){

			for(int j=0; j<arr2.length; j++){

				if(arr1[i]==arr2[j]){

					System.out.println(arr1[i]);
					break;
				}
			}
		}
	}
}
