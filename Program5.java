//WAP to take size of array from user and also take integer elements from user find the minimum element from the array
//input: Enter size: 5
//Enter array elements: 1  2  5  0  4
//output: min element= 0

import java.util.*;
class Demo{

	public static void main(String[] args){

		Scanner sc= new Scanner(System.in);
		System.out.println("Enter the Size of Array:");
		int size= sc.nextInt();

		int arr[]=new int[size];

		System.out.println("Enter the Elements of Array:");
		for(int i=0; i<arr.length; i++){

			arr[i]=sc.nextInt();
		}

		int min=arr[0];
		for(int i=0; i<arr.length; i++){

			if(arr[i]<min)
				min=arr[i];
		}
		System.out.println("Minimum Element of array = "+min);
	}
}
