// WAP to merge two given arrays.
// Array1=[10,20,30,40,50]
// Array2=[9,18,27,36,45]
// output: Merged Array=[10,20,30,40,50,9,18,27,36,45]

import java.util.*;
class Demo{

	public static void main(String[] args){

		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the size of First Array:");
		int size1=sc.nextInt();

		int arr1[]=new int[size1];
		System.out.println("Enter the Elements :");
		for(int i=0; i<arr1.length; i++){

			arr1[i]=sc.nextInt();
		}
		System.out.println("Enter the size of Second Array:");
		int size2=sc.nextInt();

		int arr2[]=new int[size2];
		System.out.println("Enter the Elements :");
		for(int i=0; i<arr2.length; i++){

			arr2[i]=sc.nextInt();
		}
		int arr3[]=new int[size1+size2];
		for(int i=0; i<arr1.length; i++){

			arr3[i]=arr1[i];
		}
		for(int i=0; i<arr2.length; i++){

			arr3[size1+i]=arr2[i];
		}
		System.out.println("Merged Array is :");
		for(int i=0; i<arr3.length; i++){

			System.out.println(arr3[i]);
		}
	}
}
