// write a program to create an array of 'n' integer elements.
// where 'n' value should be taken from the user.
// insert the value from users and find the sum of all elements in the array.
// input : 6
// 		Enter the elements in Array: 2  3  6  9  5  1
// output : 26

import java.util.*;
class Demo{

	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the Size of array:");	
		int size=sc.nextInt();

		int arr[]=new int[size];
		System.out.println("Enter the Elements:");

		for(int i=0; i<arr.length; i++){
		
			arr[i]=sc.nextInt();
		}

		int sum =0;
		for(int i=0; i<arr.length; i++){

			sum+=arr[i];
		}
		System.out.println("Sum of all elements = "+ sum);
	}

}
