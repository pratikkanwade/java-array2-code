//WAP to take size of array from user and also take integer elements from user find the maximum element from the array
//input : Enter Size: 5
//Enter Elements of Array: 1 2 5 0 4
//Output : Max element : 5

import java.util.*;
class Demo{

	public static void main(String[] args){

		Scanner sc= new Scanner(System.in);
		System.out.println("Enter the size of array:");
		 int size=sc.nextInt();

		 int arr[]= new int[size];
		 System.out.println("Enter the elements of array:");
		 for(int i=0; i<arr.length; i++){

			 arr[i]=sc.nextInt();
		 }
		 int max= arr[0];
		 for(int i=0; i<arr.length; i++){

			 if(max<arr[i])
				 max=arr[i];
		 }
		 System.out.println("Maximum Number in array is : "+max);
	}
}

