//WAP to find the sum of even and odd numbers in array.
//Display the sum value.
//input : 11  12  13  14  15
//output: Odd numbers sum=39
//		Even numbers sum=26

import java.util.*;
class Demo{

	public static void main(String[] args){

		Scanner sc= new Scanner(System.in);
		System.out.println("Enter the size of Array:");
		int size=sc.nextInt();

		int arr[]= new int[size];

		System.out.println("Enter the elements of Array:");
		for(int i=0; i<arr.length; i++){

			arr[i]=sc.nextInt();
		}

		int sum=0,sum2=0;
		for(int i=0; i<arr.length; i++){

			if(arr[i]%2==0)
				sum+=arr[i];
			else
				sum2+=arr[i];
		}
		System.out.println("Odd numbers sum: "+ sum2);
		System.out.println("Even numbers sum: "+ sum);
	}
}
