//WAP to search a specific element from an array and return its index.
//input: 1  2  4  5  6
//Enter the element to search: 4
//Output: Element found at index :2

import java.util.*;
class Demo{

	public static void main(String[] args){

		Scanner sc=new Scanner(System.in);
		System.out.println("Enter the size of Array:");
		int size= sc.nextInt();

		int arr[]=new int[size];

		System.out.println("Enter the Elements of Array:");
		for(int i=0; i<arr.length; i++){

			arr[i]=sc.nextInt();
		}
		System.out.println("Enter the number to search:");
		int num = sc.nextInt();
		for(int i=0; i<arr.length; i++){

			if(num==arr[i])
				System.out.println("Element fount at index: "+ i);
		}
	}
}
